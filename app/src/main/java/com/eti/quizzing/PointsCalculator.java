package com.eti.quizzing;

/**
 * Created by pawel on 07.01.2018.
 */

/**
 * Oblicza ze wzoru ilości punktów do ukończenia konkretnego poziomu.
 */
public class PointsCalculator {

    /**
     * Oblicza ilości punktów do ukończenia konkretnego poziomu.
     * @param i - poziom.
     * @return ilość punktów niezbędną do osiągnięcia danego poziomu.
     */
    public static int calculatePointsToLevel(int i) {
        return i * 1000 + ((10 * i) * (10 * i));
    }
}
