package com.eti.quizzing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.eti.quizzing.quiz.QuizType;
import com.eti.quizzing.util.QuestionsHolder;

import java.util.logging.Level;
import java.util.logging.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Pierwszy widok aplikacji który pojawia się tylko jeśli aplikacja nie była wcześniej uruchamiana.
 * Po załadowaniu użytkownikowi zadawane jest pytanie o poziom trudności.
 *
 * Jeśli wybierzemy poziom >= 6 to aplikacja przeprowadzi nas przez proces weryfikacji umiejętności.
 * Jeśli nie odpowiemy na min 6 z 10 pytań, to zasugeruje nam zmianę poziomu trudności.
 */
public class StartupActivity extends AppCompatActivity {

    @BindView(R.id.seekBar)
    protected SeekBar seekBar;

    @BindView(R.id.text_chosenLevel)
    protected TextView labelChosenLevel;

    @BindView(R.id.button_next)
    protected Button buttonNext;

    /**
     * Sprawdzenie i zweryfikowanie czy aplikacja jest pierwszy raz uruchamiana. Jeśli była już
     * uruchamiana to wywoływane jest przejście do kolejnego poziomu.
     * Załadowanie akcji seekBar'a.
     *
     * @param savedInstanceState - stan aplikacji.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        ButterKnife.bind(this);

        // loading questions from resources
        QuestionsHolder.INSTANCE.loadQuestions(getApplicationContext());

        // loading application
        final SharedPreferences preferences = getSharedPreferences("QUIZZING_SETTINGS", MODE_PRIVATE);
        if (preferences.contains(getResources().getString(R.string.setting_is_startup))) {
            boolean isStartup = preferences.getBoolean(getResources().getString(R.string.setting_is_startup), true);
            if (!isStartup) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "CONTINUING...");
                startMenuActivity();
            } else {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Never started this app.");
            }
        }

        // akcja nasłuchiwania zmian suwaka
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // pod wpływem ruchu seekbar'a zmieniamy poziom na etykiecie
                labelChosenLevel.setText((i + 1) + " / 10");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        /**
         * Po kliknięciu dalej zapisujemy ustawienia i przechodzimy do menu lub nasze umiejętności sąweryfikowane.
         */
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = preferences.edit();

                // zapisanie do shared preferences aktualnego poziomu.
                // dzięki temu (sharedpreferences) dane są dostępne między uruchomieniami aplikacji.

                editor.putBoolean(getResources().getString(R.string.setting_is_startup), false);
                editor.putInt(getResources().getString(R.string.setting_level), seekBar.getProgress());
                editor.putInt(getResources().getString(R.string.totalPoints), PointsCalculator.calculatePointsToLevel(seekBar.getProgress() + 1));

                if (editor.commit()) {
                    if (seekBar.getProgress() >= 5) {
                        startVerificationActivity();
                    } else {
                        startMenuActivity();
                    }
                }
            }
        });
    }

    /**
     * Startuje aktywność weryfikującą nasze umiejętności.
     */
    private void startVerificationActivity() {
        Intent intent = new Intent(this, QuizActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(getResources().getString(R.string.isVerification), true);
        intent.putExtra(getResources().getString(R.string.quiz_type), QuizType.Exercise.toString());
        startActivity(intent);
        finish();
    }

    /**
     * Startuje menu. Usuwa ze stosu aktywności tą aktywność, dzięki czemu nie ma opcji powrotu do
     * tej aktywności przez klawisz back.
     */
    private void startMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish(); // call this to finish the current activity
    }
}
