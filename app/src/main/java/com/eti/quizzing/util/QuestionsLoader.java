package com.eti.quizzing.util;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by amen on 10.12.2017.
 */

public class QuestionsLoader {
    public JSONObject loadQuestionsFromJSONFile(Context context, String fileName) {
        try {
            AssetManager manager = context.getAssets();
            InputStream file = manager.open(fileName);
            byte[] formArray = new byte[file.available()];
            file.read(formArray);
            file.close();

            String json = new String(formArray, "UTF-8");
            JSONObject jsonObject = new JSONObject(json);

            Logger.getLogger(getClass().getName()).log(Level.INFO, "Json file found and opened.");

            return jsonObject;
        } catch (IOException | JSONException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Unable to load questions from asset.");
            e.printStackTrace();
        }
        return null;
    }
}
