package com.eti.quizzing.util;

import android.content.Context;

import com.eti.quizzing.quiz.QuizQuestion;
import com.eti.quizzing.quiz.QuizType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by amen on 10.12.2017.
 */

public enum QuestionsHolder {
    INSTANCE;

    private Map<Integer, QuizQuestion> questionsDictionaryMap = new HashMap<>();
    private Map<Integer, QuizQuestion> questionsGrammarMap = new HashMap<>();
    private Map<Integer, QuizQuestion> questionsPhrasesMap = new HashMap<>();

    QuestionsHolder() {
    }

    public void loadQuestions(Context context) {
        loadDictionary(context);
        loadGrammar(context);
        loadPhrases(context);
    }

    private void loadPhrases(Context context) {
        questionsPhrasesMap.clear();
        try {
            parseQuestions(new QuestionsLoader().loadQuestionsFromJSONFile(context, "questions_phrases.json"), questionsPhrasesMap);
        } catch (JSONException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Problem with processing grammar file.");
            e.printStackTrace();
        }
    }

    private void loadGrammar(Context context) {
        questionsGrammarMap.clear();
        try {
            parseQuestions(new QuestionsLoader().loadQuestionsFromJSONFile(context, "questions_grammar.json"), questionsGrammarMap);
        } catch (JSONException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Problem with processing grammar file.");
            e.printStackTrace();
        }
    }

    private void loadDictionary(Context context) {
        questionsDictionaryMap.clear();
        try {
            parseQuestions(new QuestionsLoader().loadQuestionsFromJSONFile(context, "questions_dict.json"), questionsDictionaryMap);
        } catch (JSONException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Problem with processing dictionary file.");
            e.printStackTrace();
        }
    }

    public void parseQuestions(JSONObject questions, Map<Integer, QuizQuestion> map) throws JSONException {
        JSONArray array = questions.getJSONArray("questions");
        for (int i = 0; i < array.length(); i++) {
            JSONObject singleQuestion = array.getJSONObject(i);

            QuizQuestion question = new QuizQuestion(singleQuestion);
            System.out.println(question);

            map.put(question.getId(), question);
        }
    }

    public Map<Integer, QuizQuestion> getQuestionsDictionaryMap() {
        return questionsDictionaryMap;
    }

    public Map<Integer, QuizQuestion> getQuestionsGrammarMap() {
        return questionsGrammarMap;
    }

    public Map<Integer, QuizQuestion> getQuestionsPhrasesMap() {
        return questionsPhrasesMap;
    }

    public Collection<QuizQuestion> getQuestions(QuizType type) {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Getting and loading quiz type: " + type);
        switch (type) {
            case Phrases:
                return getPhrasesQuestions();
            case Grammar:
                return getGrammarQuestions();
            case Dictionary:
            default:
                return getDictionaryQuestions();
        }
    }

    public Collection<QuizQuestion> getDictionaryQuestions() {
        return questionsDictionaryMap.values();
    }

    public Collection<QuizQuestion> getGrammarQuestions() {
        return questionsGrammarMap.values();
    }

    public Collection<QuizQuestion> getPhrasesQuestions() {
        return questionsPhrasesMap.values();
    }
}
