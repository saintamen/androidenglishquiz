package com.eti.quizzing;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.eti.quizzing.quiz.IAnswerNotifiable;
import com.eti.quizzing.quiz.QuizQuestion;
import com.eti.quizzing.quiz.QuizType;
import com.eti.quizzing.util.QuestionsHolder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Aktywność reprezentująca quiz (zestaw quizowy)
 */
public class QuizActivity extends AppCompatActivity implements IAnswerNotifiable {

    /**
     * Reprezentuje maksymalna ilość poziomów.
     */
    private static final int LEVEL_MAX = 10;

    /**
     * Reprezentuje przelicznik maksymalnej ilości poziomów do przeskalowania.
     */
    private static final int LEVEL_SCALE = 3;

    /**
     * Zbiór wylosowanych pytań.
     */
    private Set<QuizQuestion> questionSet = new HashSet<>();

    /**
     * Iterator wylosowanych pytań.
     */
    private Iterator<QuizQuestion> iterator;

    @BindView(R.id.quiz)
    protected QuizQuestionView quiz;

    /**
     * Licznik odpowiedzi poprawnych.
     */
    private int pointsCorrect = 0;

    /**
     * Licznik odpowiedzi niepoprawnych.
     */
    private int pointsIncorrect = 0;

    /**
     * Licznik pytań.
     */
    private int questionNumber = 0;

    /**
     * Typ quizu.
     */
    private QuizType quizType;

    /**
     * Poziom użytkownika (przeskalowany).
     */
    private int userLevel = 0;


    /**
     * Przygotowuje aktywność, losuje pytania i przeskalowuje poziom użytkownika.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ButterKnife.bind(this);

        userLevel = getSharedPreferences("QUIZZING_SETTINGS", MODE_PRIVATE).getInt(getResources().getString(R.string.setting_level), 1);

        double levelProportion = userLevel;
        levelProportion /= LEVEL_MAX;
        levelProportion *= LEVEL_SCALE;
        userLevel = (int) Math.ceil(levelProportion);

        // load questions
        randomiseQuestions();

        iterator = questionSet.iterator();
        quiz.setQuestion(iterator.next());
        quiz.setQuizNotifiable(this);
    }

    /**
     * Dokonuje losowania pytań. W zależności od zbioru losowane są odpowiednie pytania. Domyślnie 5 pytań
     * dla rund, oraz 10 pytań jeśli jest to quiz weryfikujący umiejętności.
     */
    private void randomiseQuestions() {
        quizType = QuizType.valueOf(getIntent().getStringExtra(getResources().getString(R.string.quiz_type)));
        List<QuizQuestion> questions = new LinkedList<>();
        if (quizType == QuizType.Exercise) {
            questions.addAll(QuestionsHolder.INSTANCE.getQuestions(QuizType.Grammar));
            questions.addAll(QuestionsHolder.INSTANCE.getQuestions(QuizType.Phrases));
            questions.addAll(QuestionsHolder.INSTANCE.getQuestions(QuizType.Dictionary));
        } else {
            questions.addAll(QuestionsHolder.INSTANCE.getQuestions(quizType));
        }

        Random r = new Random();

        int questionsToRandomise = getResources().getInteger(R.integer.questions_in_round);
        try {
            if (getIntent().getBooleanExtra(getResources().getString(R.string.isVerification), false)) {
                questionsToRandomise = 10;
            }
        } catch (Exception e) {
            Log.wtf(getClass().getName(), "Error on getting intent: " + e, e);
        }
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Loading : " + questionsToRandomise + " questions.");
        while (questionSet.size() < questionsToRandomise) {
            // generate question number
            int questionNumber = r.nextInt(questions.size());

            QuizQuestion chosenQuestion = questions.get(questionNumber);
            if (chosenQuestion.getLevel() <= userLevel) {
                questionSet.add(chosenQuestion);
                Logger.getLogger(getClass().getName()).log(Level.INFO, "Question loaded: " + chosenQuestion + " size of questions: " + questionSet.size());
            }
        }
    }

    /**
     * Metoda wywoływana po tym jak udzielona zostanie odpowiedź.
     * @param answerCorrect - wynik, czy odpowiedź jest poprawna, czy nie.
     */
    @Override
    public void questionAnswered(boolean answerCorrect) {
        questionNumber++;

        if (answerCorrect) {
            pointsCorrect++;
        } else {
            pointsIncorrect++;
        }

        if (iterator.hasNext()) {
            if (quizType == QuizType.Exercise) {
                if (!answerCorrect) {
                    Toast.makeText(getApplicationContext(), "Incorrect!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Correct!", Toast.LENGTH_SHORT).show();
                }
            }
            Toast.makeText(getApplicationContext(), "Currently question: " + (questionNumber + 1), Toast.LENGTH_SHORT).show();
            quiz.setQuestion(iterator.next());
        } else {

            if (getIntent().getBooleanExtra(getResources().getString(R.string.isVerification), false)) {
                if (pointsCorrect < 6) {
                    showConfirmationActivity();
                } else {
                    showMenuActivity();
                }
            } else if (quizType == QuizType.Exercise) {
                showMenuActivity();
            } else {
                showResult();
            }
        }
    }

    /**
     * Uruchamia menu. (przypadek gdy wykonujemy quiz ćwiczeniowy - wtedy wynik quizu nie dodaje nam punktów)
     */
    private void showMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(getResources().getString(R.string.suggestedLevel), pointsCorrect);
        startActivity(intent);
        finish();
    }

    /**
     * Uruchamia confirmation activity. Jeśli w quizie weryfikacyjnym nie udzielilismy odpowiedniej
     * ilości poprawnych odpowiedzi, to wyświetlana jest aktywność weryfikująca.
     */
    private void showConfirmationActivity() {
        Intent intent = new Intent(this, ConfirmationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(getResources().getString(R.string.suggestedLevel), pointsCorrect);
        startActivity(intent);
        finish();
    }

    /**
     * Uruchamia wynik - activity z wynikiem quizu.
     */
    private void showResult() {
        Intent intent = new Intent(this, QuizSetResultActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(getResources().getString(R.string.incorrect_key), pointsIncorrect);
        intent.putExtra(getResources().getString(R.string.correct_key), pointsCorrect);
        startActivity(intent);
        finish();
    }
}

