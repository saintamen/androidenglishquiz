package com.eti.quizzing;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Ta aktywność wyświetla wynik całej rundy ćwiczeń.
 */
public class QuizSetResultActivity extends AppCompatActivity {

    // połączenie elementów widoków przez butterknife
    @BindView(R.id.lCurrentLevel)
    protected TextView currentLevelLabel;

    @BindView(R.id.lCorrectAnswers)
    protected TextView correctAnswersLabel;

    @BindView(R.id.lIncorrectAnswers)
    protected TextView incorrectAnswersLabel;

    @BindView(R.id.lPointsFromLevel)
    protected TextView pointsFromLevelLabel;

    @BindView(R.id.lPointsToNextLevel)
    protected TextView pointsToNextLevelLabel;

    @BindView(R.id.lTotalPoints)
    protected TextView totalPointsLabel;


    /**
     * Aktywność wyświetla wynik rundy. Po ukończeniu rundy oceniamy ilość zdobytych punktów na podstawie ilości poprawnie udzielonych odpowiedzi.
     * Po tej czynności oceniamy na którym jesteśmy poziomie i wyliczamy punkty. Wynik tej aktywności powoduje dodanie do aktualnej ilości punktów użytkownika
     * nowo zdobytych punktów.
     *
     * Suma punktów i aktualny poziom zostają zapisane w shared preferences, dzięki czemu są dostępne między uruchomieniami aplikacji.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_set_result);
        ButterKnife.bind(this);

        // current level
        SharedPreferences preferences = getSharedPreferences("QUIZZING_SETTINGS", Context.MODE_PRIVATE);

        // correct answers
        int correct = getIntent().getIntExtra(getResources().getString(R.string.correct_key), 0);
        correctAnswersLabel.setText(String.valueOf(correct));

        // incorrect answers
        int incorrect = getIntent().getIntExtra(getResources().getString(R.string.incorrect_key), 0);
        incorrectAnswersLabel.setText(String.valueOf(incorrect));

        // points from level
        int pointsFromLevel = correct * 10;
        if (correct >= 5) {
            pointsFromLevel += 100;
        }
        pointsFromLevelLabel.setText(String.valueOf(pointsFromLevel));

        // total points
        int totalPoints = preferences.getInt(getResources().getString(R.string.totalPoints), 0);
        totalPoints += pointsFromLevel;
        totalPointsLabel.setText(String.valueOf(totalPoints));

        int currentLevel = preferences.getInt(getResources().getString(R.string.setting_level), 5) + 1;

        // points to next level
        int pointsToNextLevel = PointsCalculator.calculatePointsToLevel(currentLevel + 1) - totalPoints;
        if (pointsToNextLevel <= 0) {
            currentLevel++;
            pointsToNextLevel = PointsCalculator.calculatePointsToLevel(currentLevel + 1) - totalPoints;
        }
        pointsToNextLevelLabel.setText(String.valueOf(pointsToNextLevel));
        currentLevelLabel.setText(String.valueOf((currentLevel)));

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getResources().getString(R.string.totalPoints), totalPoints);
        editor.putInt(getResources().getString(R.string.setting_level), currentLevel - 1);
        editor.apply();
    }

    /**
     * Operacja przeniesienia do aktywności głównej (menu głównego aplikacji).
     * Wraz z przeniesieniem usuwamy historię ze stosu activity, dzięki czemu użytkownik nie może wrócić do tego activity.
     */
    @OnClick(R.id.continueButton)
    public void continueClicked(View view) {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish(); // call this to finish the current activity
    }
}
