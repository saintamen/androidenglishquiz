package com.eti.quizzing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        ButterKnife.bind(this);
    }

    /**
     * Akcja na kliknięcie 'stay' powoduje zachowanie aktualnego poziomu trudności.
     *
     * @param view
     */
    @OnClick(R.id.buttonStay)
    public void stayClicked(View view) {
        startMenuActivity();
    }

    /**
     * Akcja na kliknięcie 'switch' powoduje zmianę poziomu trudności. W zależności od ilości poprawnych
     * odpowiedzi udzielonych przez użytkownika poziom trudności zostanie zmieniony na taki poziom.
     *
     * @param view
     */
    @OnClick(R.id.buttonSwitch)
    public void switchClicked(View view) {
        SharedPreferences preferences = getSharedPreferences("QUIZZING_SETTINGS", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        int suggestedLevel = getIntent().getIntExtra(getResources().getString(R.string.suggestedLevel), 5);

        editor.putBoolean(getResources().getString(R.string.setting_is_startup), false);
        editor.putInt(getResources().getString(R.string.setting_level), suggestedLevel);
        editor.putInt(getResources().getString(R.string.totalPoints), PointsCalculator.calculatePointsToLevel(suggestedLevel));

        // po zatwierdzeniu zmian (commit jest funkcją blokującą) uruchom main menu activity - aktywność menu
        if (editor.commit()) {
            startMenuActivity();
        }
    }

    /**
     * Operacja przeniesienia do aktywności głównej (menu głównego aplikacji).
     * Wraz z przeniesieniem usuwamy historię ze stosu activity, dzięki czemu użytkownik nie może wrócić do tego activity.
     */
    private void startMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish(); // call this to finish the current activity
    }
}
