package com.eti.quizzing;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eti.quizzing.quiz.QuizType;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Główne menu aplikacji.
 */
public class MainMenuActivity extends AppCompatActivity {
    @BindView(R.id.label_level)
    protected TextView labelLevel;

    /**
     * Przygotowanie widoku menu głównego (głównie ustawienie etykiety zgodnie z poziomem z sharedpreferences).
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);

        // wczytanie z shared preferences aktualnego poziomu trudności i wyświetlenie go w etykiecie
        SharedPreferences preferences = getSharedPreferences("QUIZZING_SETTINGS", Context.MODE_PRIVATE);
        int currentLevel = preferences.getInt(getResources().getString(R.string.setting_level), 5);
        labelLevel.setText((currentLevel + 1) + " / 10");
    }

    /**
     * Akcja kliknięcia dictionary powoduje uruchomienie quizu z pytaniami z tej kategorii.
     * Czynność podbindowana przez butterknife.
     */
    @OnClick(R.id.btn_dict)
    public void onDictionaryClicked(View view) {
        Intent intent = new Intent(MainMenuActivity.this, QuizActivity.class);
        intent.putExtra(getResources().getString(R.string.isVerification), false);
        intent.putExtra(getResources().getString(R.string.quiz_type), QuizType.Dictionary.toString());
        startActivity(intent);
    }


    /**
     * Akcja kliknięcia dictionary powoduje uruchomienie quizu z pytaniami z tej kategorii.
     * Czynność podbindowana przez butterknife.
     */
    @OnClick(R.id.btn_phrases)
    public void onPhrasesClicked(View view) {
        Intent intent = new Intent(MainMenuActivity.this, QuizActivity.class);
        intent.putExtra(getResources().getString(R.string.isVerification), false);
        intent.putExtra(getResources().getString(R.string.quiz_type), QuizType.Phrases.toString());
        startActivity(intent);
    }


    /**
     * Akcja kliknięcia dictionary powoduje uruchomienie quizu z pytaniami z tej kategorii.
     * Czynność podbindowana przez butterknife.
     */
    @OnClick(R.id.btn_exercises)
    public void onExerciseClicked(View view) {
        Intent intent = new Intent(MainMenuActivity.this, QuizActivity.class);
        intent.putExtra(getResources().getString(R.string.isVerification), false);
        intent.putExtra(getResources().getString(R.string.quiz_type), QuizType.Exercise.toString());
        startActivity(intent);
    }

    /**
     * Akcja kliknięcia dictionary powoduje uruchomienie quizu z pytaniami z tej kategorii.
     * Czynność podbindowana przez butterknife.
     */
    @OnClick(R.id.btn_grammar)
    public void onGrammarClicked(View view) {
        Intent intent = new Intent(MainMenuActivity.this, QuizActivity.class);
        intent.putExtra(getResources().getString(R.string.isVerification), false);
        intent.putExtra(getResources().getString(R.string.quiz_type), QuizType.Grammar.toString());
        startActivity(intent);
    }
}
