package com.eti.quizzing;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eti.quizzing.quiz.IAnswerNotifiable;
import com.eti.quizzing.quiz.QuestionType;
import com.eti.quizzing.quiz.QuizQuestion;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by amen on 10.12.2017.
 */

/**
 * Podwidok. W widoku quizu zmienia się tylko element quizquestionview któwy wyświetla pytanie.
 * Ta klasa reprezentuje ten widok.
 */
public class QuizQuestionView extends RelativeLayout {

    @BindView(R.id.answerField)
    protected EditText answerField;

    @BindView(R.id.radiogroup)
    protected RadioGroup radioGroup;

    @BindView(R.id.question_identifier)
    protected TextView labelId;

    @BindView(R.id.question_content)
    protected TextView labelContent;

    @BindView(R.id.question_answers_layout)
    protected LinearLayout answersLayout;

    @BindView(R.id.question_answer_layout)
    protected LinearLayout answerLayout;

    private QuizQuestion currentQuestion;
    private List<RadioButton> radioButtons;

    // referencja do aktywności quizu
    private IAnswerNotifiable notifiable;

    public QuizQuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.quiz_question_view, this);
        ButterKnife.bind(this);

        radioButtons = new LinkedList<>();
    }

    /**
     * Powoduje przełączenie (załadowanie) kolejnego pytania
     * @param question - pytanie do załadowania.
     */
    public void setQuestion(QuizQuestion question) {
        currentQuestion = question;

        labelId.setText("Question: " + question.getId());
        labelContent.setText(question.getQuestion());

        if (question.getType() == QuestionType.SELECT) {
            answerLayout.setVisibility(GONE);
            answersLayout.setVisibility(VISIBLE);
            radioGroup.removeAllViews();

            radioButtons.clear();
            for (int i = 0; i < question.getAnswers().length; i++) {
                RadioButton view = new RadioButton(getContext());
                view.setText(question.getAnswers()[i]);

                radioGroup.addView(view);
                radioButtons.add(view);
            }

        } else if (question.getType() == QuestionType.REWRITE) {
            answersLayout.setVisibility(GONE);
            answerLayout.setVisibility(VISIBLE);

            answerField.setText("");
        }
    }

    /**
     * Ustawia referencję na aktywność.
     * @param notifiable - referencja na aktywność.
     */
    public void setQuizNotifiable(IAnswerNotifiable notifiable) {
        this.notifiable = notifiable;
    }

    /**
     * Akcja zbindowana butterknifem. Po kliknięciu guzika przechodzimy do kolejnego pytania. Przekazujemy
     * do aktywności wynik odpowiedzi użytkownika, a następnie aktywność ładuje nowe pytanie.
     * @param view - nieużywane / element wywołujący
     */
    @OnClick(R.id.confirmButton)
    public void onConfirmClicked(View view) {
        if (currentQuestion.getType() == QuestionType.SELECT) {
            int selectedAnswer = -1;
            for (int i = 0; i < radioButtons.size(); i++) {
                if (radioButtons.get(i).isChecked()) {
                    selectedAnswer = i;
                    break;
                }
            }
            if (selectedAnswer == -1) {
                Toast.makeText(getContext(), "You have to select at least one answer.", Toast.LENGTH_LONG).show();
                return;
            }
            notifiable.questionAnswered(currentQuestion.getRightAnswerIndex() == selectedAnswer);
        } else if (currentQuestion.getType() == QuestionType.REWRITE) {
            String answerText = answerField.getText().toString();
            answerText = answerText.replace("\n", "").trim().toLowerCase();
            String[] answerWords = answerText.replace("  ", " ").replace(".", "").replace(",", "").split(" ");

            for (String correctAnswer : currentQuestion.getAnswers()) {
                String[] correctWords = correctAnswer.toLowerCase().trim().replace(",", "").replace(".", "").split(" ");

                if (correctWords.length != answerWords.length) {
                    continue;
                }

                boolean answeredCorrectly = true;
                for (int i = 0; i < correctWords.length; i++) {
                    if (!correctWords[i].equals(answerWords[i])) {
                        answeredCorrectly = false;
                        break;
                    }
                }
                if (answeredCorrectly) {
                    notifiable.questionAnswered(true);
                }
            }
            notifiable.questionAnswered(false);
        }
    }
}
