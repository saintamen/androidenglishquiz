package com.eti.quizzing.quiz;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by amen on 10.12.2017.
 */

/**
 * Reprezentuje pojedyncze pytanie quizu.
 */
public class QuizQuestion {
    // id pytania
    private int id;

    // poziom trudności pytania
    private int level;

    // pytanie
    private String question;

    // typ pytania
    private QuestionType type;

    // możliwe odpowiedzi
    private String[] answers;

    // poprawna odpowiedź (indeks)
    private int rightAnswerIndex;

    public QuizQuestion() {
    }

    public QuizQuestion(int id, QuestionType type, int level, String question, String[] answers, int rightAnswerIndex) {
        this.id = id;
        this.type = type;
        this.level = level;
        this.question = question;
        this.answers = answers;
        this.rightAnswerIndex = rightAnswerIndex;
    }

    /**
     * Konstruktor ładujący pola z obiektu JSON.
     *
     * @param singleQuestion - obiekt json pytania.
     * @throws JSONException - exception możliwy jeśli format obiektu json jest niepoprawny.
     */
    public QuizQuestion(JSONObject singleQuestion) throws JSONException {
        this.id = singleQuestion.getInt("id");
        this.level = singleQuestion.getInt("level");
        this.type = QuestionType.valueOf(singleQuestion.getString("type").toUpperCase());
        this.question = singleQuestion.getString("question");
        this.rightAnswerIndex = singleQuestion.getInt("rightAnswerIndex");

        // answers loading
        JSONArray answers = singleQuestion.getJSONArray("answers");
        this.answers = new String[answers.length()];

        // reading them all
        for (int i = 0; i < answers.length(); i++) {
            this.answers[i] = String.valueOf(answers.get(i));
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public int getRightAnswerIndex() {
        return rightAnswerIndex;
    }

    public void setRightAnswerIndex(int rightAnswerIndex) {
        this.rightAnswerIndex = rightAnswerIndex;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "QuizQuestion{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answers=" + Arrays.toString(answers) +
                ", rightAnswerIndex=" + rightAnswerIndex +
                '}';
    }
}
