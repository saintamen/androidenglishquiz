package com.eti.quizzing.quiz;

/**
 * Created by pawel on 07.01.2018.
 */

/**
 * Reprezentuje typ pytania
 * SELECT - pytanie z odpowiedziami do wybory
 * REWRITE - pytanie z otwartą odpowiedzią.
 * LINK - pytanie z łączeniem odpowiedzi ze soba w pary.
 */
public enum QuestionType {
    SELECT, REWRITE, LINK
}
