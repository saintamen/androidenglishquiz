package com.eti.quizzing.quiz;

/**
 * Created by amen on 10.12.2017.
 */

/**
 * Reprezentuje typ quizu.
 * Grammar - quiz gramatyczny
 * Dictionary - quiz słownikowy
 * Phrases - quiz z fraz
 * Exercise - quiz mieszany
 */
public enum QuizType {
    Grammar, // pytania z gramatyki
    Dictionary, // pytania słownikowe
    Phrases, // pytania o frazy
    Exercise; // ćwiczenia quizowe (mieszanka różnych kategorii)
}
