package com.eti.quizzing.quiz;

import android.content.Context;

/**
 * Created by pawel on 07.01.2018.
 */

/**
 * Interfejs wiążący aktywność quizu z jego podelementem - widokiem pytania.
 */
public interface IAnswerNotifiable {
    /**
     * Kiedy odpowiedź zostanie udzielona.
     * @param answerCorrect - wynik, czy odpowiedź jest poprawna, czy nie.
     */
    void questionAnswered(boolean answerCorrect);
}
